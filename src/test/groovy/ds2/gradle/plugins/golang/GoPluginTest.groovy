package ds2.gradle.plugins.golang

import org.gradle.api.Project
import org.gradle.internal.impldep.org.testng.annotations.Test
import org.gradle.testfixtures.ProjectBuilder

/**
 * Created by dstrauss on 07.07.16.
 */
class GoPluginTest {

        @Test
        public void greeterPluginAddsGreetingTaskToProject() {
            Project project = ProjectBuilder.builder().build()
            project.pluginManager.apply 'org.samples.greeting'
            assertTrue(project.tasks.hello instanceof GradleGoPlugin)
    }



}
