package ds2.gradle.plugins.golang;

import org.gradle.api.file.FileTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by dstrauss on 13.07.16.
 */
public class GoBuildTask extends BasicGoTask {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public GoBuildTask() {
        super();
        mustRunAfter(TaskNames.goClean.name());
    }

    @Override
    protected void beforeExec(GoEnvironmentData ged) {
        setWorkingDir(ged.getSrcDir());
    }

    @Override
    protected List<String> createArgs(GoEnvironmentData ged) {
        LOG.debug("Creating args for build..");
        List<String> rc = new ArrayList<>(10);
        rc.add("build");
        rc.addAll(getBuildFlags(ged));
        String singleModuleName = getLastNameOfModule(ged);
        if (singleModuleName != null) {
            rc.add("-o");
            rc.add(ged.getBuildDir().toPath().resolve("bin").resolve(singleModuleName).toString());
        }
        if (ged.hasSourceFiles()) {
            rc.addAll(toSourceFiles(ged.getSources()));
        } else if (ged.hasModules()) {
            rc.addAll(ged.getModules());
        }
        return rc;
    }

    private Collection<? extends String> toSourceFiles(FileTree sources) {
        Set<File> files = sources.getFiles();
        List<String> rc = new ArrayList<>();
        for (File f : files) {
            try {
                rc.add(f.getCanonicalPath().toString());
            } catch (IOException e) {
                LOG.warn("Error when creating the path for {}", f, e);
            }
        }
        return rc;
    }

    @Override
    protected String getContextDescription(GoEnvironmentData goData) {
        return "builds the go sources";
    }
}
