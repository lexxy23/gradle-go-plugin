package ds2.gradle.plugins.golang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dstrauss on 13.07.16.
 */
public class GoCleanTask extends BasicGoTask {

    @Override
    protected List<String> createArgs(GoEnvironmentData ged) {
        List<String> rc = new ArrayList<>(10);
        rc.add("clean");
        rc.addAll(getBuildFlags(ged));
        if (ged.hasModules()) {
            rc.addAll(ged.getModules());
        }
        return rc;
    }

    @Override
    protected String getContextDescription(GoEnvironmentData goData) {
        return "cleans the bin directory";
    }
}
