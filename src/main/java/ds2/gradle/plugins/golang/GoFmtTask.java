package ds2.gradle.plugins.golang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dstrauss on 08.07.16.
 */
public class GoFmtTask extends BasicGoTask {

    public GoFmtTask() {
        super();
        mustRunAfter(TaskNames.goClean.name());
    }

    @Override
    protected List<String> createArgs(GoEnvironmentData ged) {
        List<String> rc = new ArrayList<>(10);
        rc.add("fmt");
        if (ged.getModules() != null && !ged.getModules().isEmpty()) {
            rc.addAll(ged.getModules());
        }
        return rc;
    }

    @Override
    protected String getContextDescription(GoEnvironmentData ged) {
        return "to format all source files found in " + ged.getSrcDir();
    }

}
