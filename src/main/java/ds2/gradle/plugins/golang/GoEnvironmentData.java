package ds2.gradle.plugins.golang;

import org.gradle.api.Project;
import org.gradle.api.file.FileTree;
import org.gradle.api.file.SourceDirectorySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dstrauss on 07.07.16.
 */
public class GoEnvironmentData {
    public static final String EXTENSION = "go";
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private File goPath;
    private File srcDir;
    private File pkgDir;
    private File buildDir;
    private FileTree sources;
    private List<String> modules = new ArrayList<>(1);
    private SourceDirectorySet sources2;
    private BuildMode buildMode;

    public void prefillValues(Project p) {
        if (!needsConfiguration()) {
            LOG.debug("No prefill needed, all is configured so far");
            return;
        }
        LOG.debug("Starting with dummy init");
        if (goPath == null) {
            goPath = p.getProjectDir();
            LOG.debug("Setting goPath to {}", goPath);
        }
        if (srcDir == null) {
            srcDir = goPath.toPath().resolve("src").toFile();
            LOG.debug("Setting srcDir to {}", srcDir);
        }
        if (pkgDir == null) {
            pkgDir = goPath.toPath().resolve("pkg").toFile();
            LOG.debug("Setting pkgDir to {}", pkgDir);
        }
        if (sources == null) {
            sources = null;
        }
        if (buildDir == null) {
            buildDir = p.getBuildDir();
        }
        LOG.debug("Done with init");
    }

    public BuildMode getBuildMode() {
        return buildMode;
    }

    public void setBuildMode(BuildMode buildMode) {
        this.buildMode = buildMode;
    }

    public boolean needsConfiguration() {
        return goPath == null || srcDir == null || pkgDir == null;
    }

    public File getGoPath() {
        return goPath;
    }

    public void setGoPath(File goPath) {
        this.goPath = goPath;
    }

    public File getSrcDir() {
        return srcDir;
    }

    public void setSrcDir(File srcDir) {
        this.srcDir = srcDir;
    }

    public File getPkgDir() {
        return pkgDir;
    }

    public void setPkgDir(File pkgDir) {
        this.pkgDir = pkgDir;
    }

    public FileTree getSources() {
        return sources;
    }

    public void setSources(FileTree sources) {
        this.sources = sources;
    }

    public List<String> getModules() {
        return modules;
    }

    public GoEnvironmentData modules(List<String> l) {
        setModules(l);
        return this;
    }

    public GoEnvironmentData srcDir(File f) {
        setSrcDir(f);
        return this;
    }

    public GoEnvironmentData buildMode(BuildMode f) {
        setBuildMode(f);
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GoEnvironmentData{");
        sb.append("goPath=").append(goPath);
        sb.append(", srcDir=").append(srcDir);
        sb.append(", pkgDir=").append(pkgDir);
        sb.append(", sources=").append(sources);
        sb.append(", modules=").append(modules);
        sb.append(", sources2=").append(sources2);
        sb.append(", buildMode=").append(buildMode);
        sb.append('}');
        return sb.toString();
    }

    public File getBuildDir() {
        return buildDir;
    }

    public void setBuildDir(File buildDir) {
        this.buildDir = buildDir;
    }

    public GoEnvironmentData buildDir(File f) {
        setBuildDir(f);
        return this;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public boolean hasModules() {
        return modules != null && !modules.isEmpty();
    }

    public boolean hasSourceFiles() {
        return sources != null && sources.getFiles() != null;
    }
}
