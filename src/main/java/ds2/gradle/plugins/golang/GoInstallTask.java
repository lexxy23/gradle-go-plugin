package ds2.gradle.plugins.golang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dstrauss on 13.07.16.
 */
public class GoInstallTask extends BasicGoTask {

    public GoInstallTask() {
        super();
        mustRunAfter(TaskNames.goClean.name());
    }

    @Override
    protected List<String> createArgs(GoEnvironmentData ged) {
        List<String> rc = new ArrayList<>(10);
        rc.add("install");
        rc.addAll(getBuildFlags(ged));
        if (ged.hasModules()) {
            rc.addAll(ged.getModules());
        }
        return rc;
    }

    @Override
    protected String getContextDescription(GoEnvironmentData goData) {
        return "installs the packages in " + goData.getSrcDir();
    }
}
