package ds2.gradle.plugins.golang;

public enum BuildMode {
    ARCHIVE("archive"),
    C_ARCHIVE("c-archive"),
    C_SHARED("c-shared"),
    DEFAULT("default"),
    SHARED("shared"),
    EXE("exe"),
    PIE("pie"),
    PLUGIN("plugin"),;
    private String mode;

    BuildMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }
}