package ds2.gradle.plugins.golang;

import groovy.lang.Closure;
import org.gradle.api.Task;
import org.gradle.api.tasks.Exec;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.TaskExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by dstrauss on 13.07.16.
 */
public abstract class BasicGoTask extends Exec {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public Task configure(Closure closure) {
        Task rc = super.configure(closure);
        GoEnvironmentData ged = getGoData();
        performConfiguration(ged);
        return rc;
    }

    protected void performConfiguration(GoEnvironmentData ged) {
        LOG.debug("Here we perform some initial configuration..");
    }

    private void validateModules(GoEnvironmentData ged) {
        LOG.debug("Validating modules in GED");
        if (ged.hasModules()) {
            for (String module : ged.getModules()) {
                LOG.debug("Checking module {}", module);
                Path modulePath = ged.getSrcDir().toPath().resolve(module);
                LOG.debug("ModulePath should be {}", modulePath);
                if (!Files.exists(modulePath)) {
                    throw new IllegalArgumentException("Given module path " + modulePath + " does not exist!");
                }
                if (!Files.isDirectory(modulePath)) {
                    throw new IllegalArgumentException("Given module path " + modulePath + " is not a directory!");
                }
                if (!Files.isReadable(modulePath)) {
                    throw new IllegalArgumentException("Given module path " + modulePath + " cannot be read!");
                }
            }
        } else {
            LOG.debug("but this project has no modules.. which somehow is strange");
        }
        LOG.debug("Done validating modules in GED");
    }

    protected abstract List<String> createArgs(GoEnvironmentData ged);

    protected Collection<? extends String> getBuildFlags(GoEnvironmentData ged) {
        List<String> rc = new ArrayList<>();
        rc.add("-pkgdir");
        rc.add(ged.getPkgDir().toString());
        rc.add("-v");
        //rc.add("-x");
        if (ged.getBuildMode() != null) {
            rc.add("-buildmode=" + ged.getBuildMode().getMode());
        }
        LOG.debug("Build flags for this env are: {}", rc);
        return rc;
    }

    @Override
    @TaskAction
    protected void exec() {
        try {
            OutputStream os = new BufferedOutputStream(System.out);
            setStandardOutput(os);
            setErrorOutput(new BufferedOutputStream(System.err));
            LOG.debug("Preparing exec for this run..");
            GoEnvironmentData ged = getGoData();
            StringBuilder sb = new StringBuilder(ged.getGoPath().toPath().toAbsolutePath().toString());
            sb.append(File.pathSeparator).append(ged.getSrcDir().toPath().toAbsolutePath().toString());
            LOG.debug("Setting up GOPATH env: {}", sb.toString());
            getEnvironment().put("GOPATH", sb.toString());
            LOG.debug("Setting new working directory");
            setWorkingDir(ged.getGoPath());
            LOG.debug("Setting arguments for the exec");
            setArgs(createArgs(ged));
            LOG.debug("Now some static references");
            setExecutable("go");
            setIgnoreExitValue(false);
            LOG.debug("Validating modules..");
            validateModules(ged);
            LOG.debug("Last point before we execute..");
            beforeExec(ged);
            LOG.debug("Arguments so far for this run: {}, will execute now", getArgs());
        } catch (Exception e) {
            LOG.error("Error when setting up the task!", e);
            throw new TaskExecutionException(this, e);
        }
        try {
            super.exec();
        } finally {
            LOG.info("Done with exec");
        }
    }

    protected void beforeExec(GoEnvironmentData ged) {
        LOG.debug("Here you can put some changes right before we execute");
    }

    ;

    protected GoEnvironmentData getGoData() {
        LOG.debug("Getting go data from project..");
        GoEnvironmentData rc = (GoEnvironmentData) getProject().getExtensions().getByName(GoEnvironmentData.EXTENSION);
        if (rc == null) {
            LOG.debug("Have to create env data");
            rc = new GoEnvironmentData();
            getProject().getExtensions().add(GoEnvironmentData.EXTENSION, rc);
        }
        rc.prefillValues(getProject());
        LOG.debug("GoEnv = {}", rc);
        return rc;
    }

    protected String getLastNameOfModule(GoEnvironmentData ged) {
        String rc = null;
        if (ged.hasSourceFiles() && ged.getSources().getFiles() != null && ged.getSources().getFiles().size() == 1) {
            rc = ged.getSources().getSingleFile().getName().toString();
            if (rc.endsWith(".go")) {
                rc = rc.substring(rc.length() - 3);
            }
        } else if (ged.hasModules() && ged.getModules().size() == 1) {
            Path p = ged.getSrcDir().toPath().resolve(ged.getModules().get(0));
            rc = p.getFileName().toString();
        }
        LOG.debug("lastNameOfModule could be {}", rc);
        return rc;
    }

    protected List<String> getSourceFilesInModule(GoEnvironmentData ged, String module) {
        List<String> rc = new ArrayList<>();
        if (ged.hasModules()) {

        }
        return rc;
    }

    @Override
    public String getDescription() {
        return getContextDescription(getGoData());
    }

    protected abstract String getContextDescription(GoEnvironmentData goData);
}
