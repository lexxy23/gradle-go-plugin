package ds2.gradle.plugins.golang;

/**
 * Created by dstrauss on 15.07.16.
 */
public enum TaskNames {
    goClean, goBuild, goInstall, goGet, goFmt;
}
