package ds2.gradle.plugins.golang

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Created by dstrauss on 07.07.16.
 */
class GradleGoPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.extensions.create(GoEnvironmentData.EXTENSION, GoEnvironmentData)
        GoEnvironmentData gpe = project.go
        if (gpe == null) {
            gpe = new GoEnvironmentData()
            gpe.prefillValues(project)
        }
        project.task(TaskNames.goClean, type: GoCleanTask)
        project.task(TaskNames.goGet, type: GoGetTask)
        project.task(TaskNames.goFmt, type: GoFmtTask, dependsOn: TaskNames.goGet.name())
        project.task(TaskNames.goBuild, type: GoBuildTask, dependsOn: TaskNames.goFmt.name())
        project.task(TaskNames.goInstall, type: GoInstallTask, dependsOn: TaskNames.goBuild.name())

        project.defaultTasks(TaskNames.goBuild.name());
    }
}
