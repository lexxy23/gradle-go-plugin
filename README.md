DS/2 OSS Gradle GoLang Plugin
=============================

A gradle plugin to build software written in the go language.

## Alternatives

Please also see other great plugins for Gradle:

* https://github.com/gogradle/gogradle

How to build this plugin
------------------------

The plugin contains a wrapper. Using it, you can perform

```
./gradlew clean build publishToMavenLocal
```

to create and install this version of the software.

How to release a version
------------------------

If you need to create a fixed version of it, you can invoke from master branch:

```
./gradlew :clean :release
```

to create a new release. The release task will ask for appropriate
things like version, tag etc.

If you use a CI environment, you may want to script the data by using

```
./gradlew :release -Prelease.useAutomaticVersion=true -Prelease.releaseVersion=1.0.0 -Prelease.newVersion=1.1.0-SNAPSHOT
```

This example above would create the 1.0.0 version of this plugin.

However, when perform releases from your computer, we highly recommend
the manual way.

# Usage

Add it via

    plugins {
        id 'ds2.gradle.plugins.golang' version '0.1.3-SNAPSHOT'
    }

or locally using

    buildscript {
        repositories {
            mavenLocal()
        }
        dependencies {
            classpath group: 'ds2.gradle.plugins.golang', name: 'gradle-go-plugin', version: '0.1.3-SNAPSHOT'
        }
    }
    
    apply plugin: 'ds2.gradle.plugins.golang'
    
    go {
        modules Arrays.asList('hello/world')
    }

